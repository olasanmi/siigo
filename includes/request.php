<?php 
    class RequestApi{

        public static function request($metodo, $url, $auth = false,$access_token = "",$json = ""){
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);

            if ($metodo == 'POST' or $metodo == 'PUT') {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $metodo);
                if ( $json != "" ){
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                }
            }

            $header = ( $auth ) ? "Authorization: ".$access_token."": "Accept: application/json";
            $partnerIdHeader = "Partner-Id: OnexMedia";
            
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json",
                $header,
                $partnerIdHeader
            ));
    
            $response = curl_exec($ch);
            curl_close($ch);

            return json_decode($response);
        } 

        public static function handlerRequest($method, $url, $type = 'siigo', $params) {
            try {
                // Clave API
                $api_key = 'lj25UuKwSm51JMiqZIToVaafOPuUMf1f1FVDBURd';
                // Configuración de la solicitud cURL
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method); // Puedes cambiar a 'POST' u otros métodos según sea necesario
                // Configurar el encabezado X-Api-Key
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'X-Api-Key: ' . $api_key
                ));
                // Realizar la solicitud
                $response = curl_exec($ch);
                // Verificar errores
                if (curl_errno($ch)) {
                    echo 'Error al realizar la solicitud cURL: ' . curl_error($ch);
                }
                // Cerrar la conexión cURL
                curl_close($ch);
                // Procesar la respuesta
                return json_decode($response, true);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
    }
?>