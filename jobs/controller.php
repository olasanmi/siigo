<?php 
    ini_set('max_execution_time', 60000);
    ini_set('max_input_time', 300000);

    require_once dirname(__DIR__).'/jobs/productos_jobs.php';


    class Controller{

        public static function ejecutar($Object, $prefijo){
            try {
                // get user and config data
                $usuario = auth::user();
                $sg_configuracion_jobs = ejecutarSQL::consultar("select * from sg_configuracion_jobs where tipo='producto'");
                if( mysqli_num_rows($sg_configuracion_jobs) != 0 ){
                    // config data
                    $configuracion_jobs = mysqli_fetch_array($sg_configuracion_jobs);
                    $configuracion_jobs = $configuracion_jobs['setting'];
                    // main query
                    // $productArr = ejecutarSQL::consultar("select ".$prefijo."posts.post_parent, ".$prefijo."posts.ID, ".$prefijo."posts.post_title, ".$prefijo."posts.post_type, ".$prefijo."postmeta.meta_value AS sku
                    // from ".$prefijo."posts LEFT JOIN ".$prefijo."postmeta  ON ".$prefijo."posts.ID = ".$prefijo."postmeta.post_id
                    // WHERE ".$prefijo."posts.post_type IN ('product', 'product_variation') AND ".$prefijo."postmeta.meta_key = '_sku'
                    // GROUP BY ".$prefijo."posts.ID");
                    $productArr = ejecutarSQL::consultar("select ".$prefijo."posts.post_parent, ".$prefijo."posts.ID, ".$prefijo."posts.post_title, ".$prefijo."posts.post_type, ".$prefijo."postmeta.meta_value AS sku
                    from ".$prefijo."posts LEFT JOIN ".$prefijo."postmeta  ON ".$prefijo."posts.ID = ".$prefijo."postmeta.post_id
                    WHERE ".$prefijo."posts.post_type IN ('product', 'product_variation') AND ".$prefijo."postmeta.meta_key = '_sku'
                    GROUP BY ".$prefijo."posts.ID, ".$prefijo."posts.post_parent, ".$prefijo."posts.post_title, ".$prefijo."posts.post_type, ".$prefijo."postmeta.meta_value;");
                    // validate num rows
                    if(mysqli_num_rows($productArr) > 0) {
                        $list = mysqli_fetch_all($productArr, MYSQLI_ASSOC);
                        foreach ($list as $key => $value) {
							 echo "Voy a filtrar el sku: " . $value['sku'] . " \n\n";
                            // set url
                            $url = "https://api.siigo.com/v1/products?code=" . $value['sku'];
                            // hacemos la peticion a siigo
                            $productSiigo = RequestApi::request('GET', $url, true, $usuario['access_token'], "");
                            
                            var_dump($productSiigo);
                            if(isset($productSiigo->results) and count($productSiigo->results) > 0) {
                                echo "encontre un producto";
                                $productData = $productSiigo->results[0];
                                $productData->wordpressSku = $value['sku'];
                                $productData->wordpressId = $value['ID'];
                                $productData->parent = $value['post_type'] == 'product_variation' ? $value['post_parent'] : $value['ID'];
                                // valdiate product.
                                echo "Nombre wordpress: " . $value['post_title'] . "\n\n"; 
                                echo "Data Siigo: El producto " . $productData->name . " con el sku " .$productData->code. " tiene un stock total de: " . $productData->available_quantity . " \n \n";
                                // save stock
                                ProductosJobs::ejecutar($Object, $prefijo, $productData, json_decode($configuracion_jobs));
                            }
                        }
                    }
                }
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }

        public static function error($error = []){

        }

    }


?>