<?php 
    ini_set('max_execution_time', 60000);
    ini_set('max_input_time', 300000);

	$date = date("Y-m-d H:i:s");
    $file = $_SERVER['PHP_SELF'];
    $message = "[$date] $file: Se ha ejecutado";
    // error_log($message . "\n", 3, "/home/biosophia/public_html/wp-content/plugins/siigoplugins/jobs/execution.log");
    error_log($message . "\n", 3, "execution.log");
    require_once dirname(__DIR__).'/includes/declaracion.php';
    require_once dirname(__DIR__).'/includes/horario.php';
    require_once dirname(__DIR__).'/includes/conexion.php';
    require_once dirname(__DIR__).'/includes/request.php';
    require_once dirname(__DIR__).'/includes/auth.php';

    if( auth::check($Object) ){
        require_once dirname(__DIR__).'/jobs/controller.php';
        $Object = $Object->format("Y-m-d h:i:s");
        Controller::ejecutar($Object, PREFIJO);
    }
?>