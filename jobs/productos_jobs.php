<?php
    ini_set('max_execution_time', 60000);
    ini_set('max_input_time', 300000);

    class ProductosJobs{

        public static function ejecutar ($fecha, $prefijo, $product, $configuracion) {
            // tablas
            $tabla = $prefijo.'wc_product_meta_lookup';
            $tabla_posts = $prefijo.'posts';
            $tabla_postmeta = $prefijo.'postmeta';

            // validate product and update
            self::actualizar_stock(
                $product->wordpressSku,
                intval($product->available_quantity),
                $tabla,
                $tabla_postmeta, 
                $product->wordpressId
            );

            // validamos el stock status al producto principal.
            if (intval($product->available_quantity) > 0) {
                consultasSQL::UpdateSQL(
                    $tabla_postmeta,
                    "meta_value = 'instock'",
                    'post_id ="'.$product->parent.'" and meta_key = "_stock_status" '
                );
            }

            // save log
            if( !self::check($product->wordpressId, $product->wordpressSku) ){
                self::save($product->wordpressId, $product->wordpressSku, json_encode($configuracion), $fecha);
            }else{
                self::update($product->wordpressId, $product->wordpressSku, json_encode($configuracion), $fecha);
            }

            // finish an log modificado en consola
            echo "Se ha modificado la data del producto: " . $product-> wordpressSku . " \n";
        }

        public static function actualizar_stock($sku, $stock, $tabla, $tabla_postmeta, $id_producto){
            $stock_status = ( $stock > 0 ) ? 'instock' : 'outofstock';
            consultasSQL::UpdateSQL(
                $tabla,
                'stock_quantity = "'.$stock.'",
                stock_status = "'.$stock_status.'"  ',
                ' sku = "'.$sku.'" '
            );
            consultasSQL::UpdateSQL(
                $tabla_postmeta,'meta_value = "'.$stock.'"',
                ' post_id ="'.$id_producto.'" and meta_key = "_stock" '
            );
            consultasSQL::UpdateSQL(
                $tabla_postmeta,
                'meta_value = "'.$stock_status.'"  ',
                ' post_id ="'.$id_producto.'" and meta_key = "_stock_status" '
            );
        }

        public static function actualizar_precio($precios_siigo, $sku, $tabla, $tabla_postmeta, $id_producto){
            foreach ($precios_siigo as $key => $item) {
                foreach ($item->price_list as $clave => $valor) {
                    $valor->value = intval($valor->value);
                    consultasSQL::UpdateSQL($tabla,'
                        min_price = "'.$valor->value.'",
                        max_price = "'.$valor->value.'"   
                    ',' sku = "'.$sku.'" ');
                    //meta_value = "'.$valor->value.'" 
                    consultasSQL::UpdateSQL($tabla_postmeta,'
                        meta_value = "'.$valor->value.'"  
                    ',' post_id ="'.$id_producto.'" and (meta_key = "_price" or meta_key = "_regular_price")');
                }
            }
        }

        public static function check($id, $sku)
        {
            $consultar = ejecutarSQL::consultar("select * from sg_productos_jobs where sku='".$sku."' and id_producto='".$id."' ");

            if( mysqli_num_rows($consultar) != 0 ){
                return true;
            }

            return false;
        }

        public static function save($id, $sku, $campos, $fecha){
            consultasSQL::InsertSQL('sg_productos_jobs','id_producto, sku, campos_actualizados,fecha_creado,fecha_editado',"
                '$id',
                '$sku',
                '$campos',
                '$fecha',
                '$fecha'
            ");
        }

        public static function update($id, $sku,$campos,$fecha){
            consultasSQL::UpdateSQL('sg_productos_jobs',"
                campos_actualizados = '$campos',
                fecha_editado = '$fecha'
            ", "sku='".$sku."' and id_producto='".$id."'");
        }

    }

?>