<?php 
include(SIIGO_RUTA.'/includes/conexion.php');
include(SIIGO_RUTA.'/includes/request.php');
include(SIIGO_RUTA.'/includes/auth.php');
require_once SIIGO_RUTA.'/includes/horario.php';

class OrderApi{
    public $departaments;

    public function __construct() {
        $this->departaments = array(
            "ANT" => array("nombre" => "ANTIOQUIA", "numero" => "05"),
            "ATL" => array("nombre" => "ATLÁNTICO", "numero" => "08"),
            "BOG" => array("nombre" => "BOGOTÁ, D.C.", "numero" => "11"),
            "BOL" => array("nombre" => "BOLÍVAR", "numero" => "13"),
            "BOY" => array("nombre" => "BOYACÁ", "numero" => "15"),
            "CAL" => array("nombre" => "CALDAS", "numero" => "17"),
            "CAQ" => array("nombre" => "CAQUETÁ", "numero" => "18"),
            "CAU" => array("nombre" => "CAUCA", "numero" => "19"),
            "CES" => array("nombre" => "CESAR", "numero" => "20"),
            "COR" => array("nombre" => "CÓRDOBA", "numero" => "23"),
            "CUN" => array("nombre" => "CUNDINAMARCA", "numero" => "25"),
            "CHO" => array("nombre" => "CHOCÓ", "numero" => "27"),
            "HUI" => array("nombre" => "HUILA", "numero" => "41"),
            "LAG" => array("nombre" => "LA GUAJIRA", "numero" => "44"),
            "MAG" => array("nombre" => "MAGDALENA", "numero" => "47"),
            "MET" => array("nombre" => "META", "numero" => "50"),
            "NAR" => array("nombre" => "NARIÑO", "numero" => "52"),
            "NSA" => array("nombre" => "NORTE DE SANTANDER", "numero" => "54"),
            "QUI" => array("nombre" => "QUINDÍO", "numero" => "63"),
            "RIS" => array("nombre" => "RISARALDA", "numero" => "66"),
            "SAN" => array("nombre" => "SANTANDER", "numero" => "68"),
            "SUC" => array("nombre" => "SUCRE", "numero" => "70"),
            "TOL" => array("nombre" => "TOLIMA", "numero" => "73"),
            "VCA" => array("nombre" => "VALLE DEL CAUCA", "numero" => "76"),
            "ARA" => array("nombre" => "ARAUCA", "numero" => "81"),
            "CAS" => array("nombre" => "CASANARE", "numero" => "85"),
            "PUT" => array("nombre" => "PUTUMAYO", "numero" => "86"),
            "SAP" => array("nombre" => "ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SANTA CATALINA", "numero" => "88"),
            "AMA" => array("nombre" => "AMAZONAS", "numero" => "91"),
            "GUA" => array("nombre" => "GUAINÍA", "numero" => "94"),
            "GUV" => array("nombre" => "GUAVIARE", "numero" => "95"),
            "VAU" => array("nombre" => "VAUPÉS", "numero" => "97"),
            "VIC" => array("nombre" => "VICHADA", "numero" => "99")
        );
    }

    /**
     * envio el cliente a siigo
     */
    public function sendOrder($orderId, $newStatus){
        try {
            $Object = new DateTime();
            $Object->setTimezone(new DateTimeZone('America/Bogota'));
            $auth = auth::check($Object);
            $user = auth::user();
            auth::update($user['username'], $user['api_key'], $Object);
            $sg_configuracion_jobs = ejecutarSQL::consultar("select * from sg_configuracion_jobs where tipo='pedidos'");
            if (mysqli_num_rows($sg_configuracion_jobs) != 0) {
                $configuration = mysqli_fetch_array($sg_configuracion_jobs);
                $setting = json_decode($configuration['setting'], true);
                $siigoId = get_post_meta($orderId, 'siigo_id', true);
                // get dni
                $dni = get_post_meta($orderId, 'billing_number', true);
				if(!$dni) {
					$dni = get_post_meta($orderId, 'billing_cedula', true);
				}
				$typeDocument = get_post_meta($orderId, 'billing_id_tipe', true);
                // get melon da tabilling_id_tipe
                $orderIdSetting = $dni ? $orderId : '1829';
                $urlMelonOrder = 'https://api.melonn.com/prod/api/sell-orders/' . $orderIdSetting; // cambiar el numero por el orderId
                $melonOrderData = RequestApi::handlerRequest('GET', $urlMelonOrder, 'siigo', null);
                $selectedWhereHouse = '';
                if (isset($melonOrderData['warehouse'])) {
                    $whereHouse = $melonOrderData['warehouse']['name'];
                    $urlWherehouse = 'https://api.melonn.com/prod/api/logistics/warehouses';
                    $melonWherehouseData = RequestApi::handlerRequest('GET', $urlWherehouse, 'siigo', null);
                    foreach ($melonWherehouseData as $key => $whereh) {
                        if ($whereh['displayName'] == $whereHouse) {
                            $selectedWhereHouse = $whereh['displayName'];
                        }
                    }
                }
                if ($setting['status'] != $newStatus) {
                    return wp_send_json(array(
                        'status' => 'error',
                        'message' => 'El estado de la orden no es igual a ' . $setting['status'],
                    ));
                }
                if ($setting['status'] === $newStatus and $siigoId == '') {
                    // set url siigo
                    $url = "https://api.siigo.com/v1/customers";
                    $usuario = auth::user();
                    // get ordser data
                    $order = wc_get_order($orderId);
                    $orderData = $order->get_data();
                    // get client
                    $stateExplode = explode('-', $order->get_billing_state());
                    // get state
                    $state = '05';
                    if (isset($state[1])) {
						$codeState = isset($stateExplode[1]) ? $stateExplode[1] : $stateExplode[0];
                        $code = $this->getDepartamentNumber($codeState);
                        if ($code) {
                            $state = $code;
                        }
                    }
                    // get city
                    $city = '05001';
                    $cityExplode = $order->get_billing_city();
                    $cityData = $this->getCity($cityExplode, $state);
                    if ($cityData) {
                        $city = $cityData;
                    }
                    // storages
                    $urlStorages = 'https://api.siigo.com/v1/warehouses';
                    $storagesData = RequestApi::request('GET', $urlStorages, true, $usuario['access_token'], null);
                    $filteredStorage = array_filter($storagesData, function ($storage) use ($selectedWhereHouse) {
                        if (strtolower($storage->name) === strtolower($selectedWhereHouse)) {
                            return $storage->name;
                        };
                    });
                    $storage = null;
                    if (count($filteredStorage) > 0) {
                        foreach($filteredStorage as $key => $valueS) {
                            $storage = $valueS->id;
                        }
                    }
                    // storages
                    // set dni
                    $dni = $dni ? $dni : '1036987548';
					// list taxes
					$taxesUrl = 'https://api.siigo.com/v1/taxes';
					$bioshopiaTaxes = RequestApi::request('GET', $taxesUrl, true, $usuario['access_token'], null);
                    // validamos si existe el usuario por dni
                    $issetOnSiigo = RequestApi::request('GET', $url . '?identification=' . $dni, true, $usuario['access_token'], null);
                    $issetClient = json_decode(json_encode($issetOnSiigo, true), true);
                    // procedemos a crearlo en siigo
                    $client = [
                        'type' => 'Customer',
                        'person_type' => 'Person',
                        'id_type' => !$typeDocument ? '13' : $typeDocument,
                        'name' => [
                            strtoupper($order->get_billing_first_name()),
                            strtoupper($order->get_billing_last_name())
                        ],
                        'fiscal_responsibilities' => [
                            [
                                'code' => 'R-99-PN'
                            ]
                        ],
                        'identification' => $dni,
                        'active' => true,
                        'address' => [
                            'address' => $order->get_billing_address_1() . ', ' . $order->get_billing_address_2() . '. ',
                            'city' => [
                                'country_code' => 'CO',
                                'state_code' => $state,
                                'city_code' => $city,
                            ]
                        ],
						'phones' => [
							[
								'indicative' => '57',
                                'number' => str_replace(' ', '', $order->get_billing_phone()),
							]
						],
                        'contacts' => [
                            [
                                'first_name' => $order->get_billing_first_name(),
                                'last_name' => $order->get_billing_last_name(),
                                'email' => $orderData['billing']['email'],
                                'phone' => [
                                    'indicative' => '57',
                                    'number' => str_replace(' ', '', $order->get_billing_phone()),
                                ]
                            ]
                        ],   
                    ];
                    // do request to clients
                    if (isset($issetClient['results']) and count($issetClient['results']) > 0) {
                        $response = RequestApi::request('PUT', $url . '/' . $issetClient['results'][0]['id'], true, $usuario['access_token'], json_encode($client, true));
                    } else {  
                        $response = RequestApi::request('POST', $url, true, $usuario['access_token'], json_encode($client, true));
                    }
                    if ($response->Status && $response->Status === 400) {
                        $messsage = $response->Errors[0]->Message . ' ' . $response->Errors[0]->Params[0];
                        $order->add_order_note('Siigo error: ' . $messsage);
                        $order->save();
                        return false;
                    }
                    // save client in our bbdd
                    if (isset($client['identification']) and !is_null($client['identification'])) {                    
                        // data
                        $date = date("Y-m-d h:i:s");
                        $name = $order->get_billing_first_name();
                        $lastName = $order->get_billing_last_name();
                        $dni = $client['identification'];
                        // validamos si existe el liente
                        $issetClient = ejecutarSQL::consultar("select * from sg_clients_jobs where dni='". $dni ."'");
                        if (mysqli_num_rows($issetClient) != 0) {
                            $clientBd = mysqli_fetch_array($issetClient);
                            consultasSQL::UpdateSQL('sg_clients_jobs', "
                                nombre = '".$name."',
                                apellido = '".$lastName."'
                            ", 'dni = "' .$dni. '"');
                        } else {
                            // save new data
                            consultasSQL::InsertSQL(
                                'sg_clients_jobs',
                                'nombre, apellido, dni, fecha_creado',"
                                '$name',
                                '$lastName',
                                '$dni',
                                '$date'
                            ");
                        }
                    }
                    // send order data
                    $invoice = $this->sendOrderdata($client['identification'], $order, $storage);
                    return wp_send_json(array(
                        'status' => isset($invoice['id']) ? 'success' : 'error',
                        'message' => 'Se ha generado la factura correctamente la referencia es: ' . $invoice['name'],
                        'data' => $invoice
                    ));
                }
            }
        } catch (\Exception $e) {
            error_log($e->getMessage() . "\n", 3, "execution.log");
        }
    }

    // send order data
    public function sendOrderdata($dni, $orderBd, $storage) {
        $sg_configuracion_jobs = ejecutarSQL::consultar("select * from sg_configuracion_jobs where tipo='pedidos'");
        if (mysqli_num_rows($sg_configuracion_jobs) != 0) {
            // set url
            $usuario = auth::user();
            $url = "https://api.siigo.com/v1/invoices";
            // get config order data
            $configuration = mysqli_fetch_array($sg_configuracion_jobs);
            $setting = json_decode($configuration['setting'], true);
            $orderData = $orderBd->get_data();
			$date = $orderBd->get_date_created();
			if ($date) {
				$date = $date->format('Y-m-d');
			}
            // prepare order
            $order = [
                'document'=> [
                    'id' => $setting['document_id'], 
                ],
                'date' => isset($date) ? $date : date("Y-m-d"),
                'customer' => [
                    'identification' => $dni,
                ],
                'seller' => $setting['seller'],
                // 'cost_center' => $setting['cost_center'],
                'stamp' => [
                    'send' => false,
                ],
                'mail' => [
                    'send' => false,
                ],
                'observations' => isset($orderData['customer_note']) ? $orderData['customer_note'] : '',
                'items' => [],
                'payments' => []
            ];
            // get delivered
            $delivery = $orderBd->get_total_shipping(); // validar que hacer en reunion
            $discountTotal = 0;
            // set items in orders
            foreach ($orderBd->get_items() as $itemId => $item) {
                $product = $item->get_product();
                $order_item_data = $item->get_data();
                // Obtener el subtotal y el subtotal del impuesto
                $subtotal = round($order_item_data['subtotal']);
                $subtotal_tax = round($order_item_data['subtotal_tax']);
                $priceInLineSell = $subtotal + $subtotal_tax;
                $priceSell = $product->get_price();
                if($priceInLineSell != $product->get_price()) {
                    $priceSell = $priceInLineSell;
                }
                
                $data['code'] = $product->get_sku();
                $data['price'] = $priceSell;
                $data['taxed_price'] = $priceSell;
                $data['description'] = $product->get_name();
                $data['quantity'] = $item->get_quantity();
                $data['taxes'] = [
                    [
                        'id' => '7003' // cambiar el impuesto por el de bio sophia
                    ]
                ];
                $subtotal = $item->get_subtotal();
                $total = $item->get_total();
                $data['warehouse'] = $storage;
                $discountAmount = $subtotal - $total;
                $discountTotal = $discountTotal + $discountAmount;
                if ($subtotal != 0 and $subtotal != $total and $discountAmount > 0) {
                    $discountPercentage = ($discountAmount / $subtotal) * 100;
                    $data['discount'] = round($discountPercentage, 2);
                }
                if (intval($discountTotal) == 0 and $item->get_total() < $product->get_price()) {
                    $baseImponible = round($product->get_price() / 1.19, 2);
                    $porcentDiscount = $this->calcularPorcentajeDescuento($baseImponible, $item->get_total());
                    if ($porcentDiscount > 0) {
                        $data['discount'] = $porcentDiscount;
                    }
                }
                array_push($order['items'], $data);
            }
            // shipping data
            $dataDiscount['code'] = 'Envios';
            $dataDiscount['price'] = round($delivery, 2);
            $dataDiscount['taxed_price'] = round($delivery, 2);
            $dataDiscount['description'] = 'Envios';
            $dataDiscount['quantity'] = 1;
            $dataDiscount['taxes'] = [
                [
                    'id' => '7003' // cambiar el impuesto por el de bio sophia
                ]
            ];
            // set payment data
            $payment = $orderBd->get_payment_method() == 'cod' ? 2999 : 8129;
            $order['payments'] = [
                [
                    'id' => $payment,
                    'value' => round($orderBd->get_total(), 2),
                    'due_date' => date("Y-m-d"),
                ]
            ];
            array_push($order['items'], $dataDiscount);

            // get order meta
            $siigoId = get_post_meta($orderData['id'], 'siigo_id', true);
            // do request
            if (isset($siigoId) and $siigoId == '') {
                $response = RequestApi::request('POST', $url, true, $usuario['access_token'], json_encode($order, true));
                $dataSiigo = json_decode(json_encode($response, true), true);
                if ($response->Status && $response->Status === 400) {
                    $messsage = $response->Errors[0]->Message . ' ' . $response->Errors[0]->Params[0];
                    $orderBd->add_order_note('Siigo error: ' . $messsage);
                    $orderBd->save();
                    return false;
                }
                // set meta data in order
                if (isset($dataSiigo['id']) and isset($dataSiigo['number']) and isset($dataSiigo['name'])) {
                    update_post_meta($orderData['id'], 'siigo_id', $dataSiigo['id'], true);
                    update_post_meta($orderData['id'], 'siigo_number', $dataSiigo['number'], true);
                    update_post_meta($orderData['id'], 'siigo_name', $dataSiigo['name'], true);
                    // insert register on main tables
                    $orderid = $orderData['id'];
                    $siigoId = $dataSiigo['name'];
                    $date = date("Y-m-d h:i:s");
                    consultasSQL::InsertSQL(
                        'sg_pedidos_jobs',
                        'id_oder, siigo_reference, fecha_creado, fecha_editado, client_dni',
                        "
                            '$orderid',
                            '$siigoId',
                            '$date',
                            '$date',
                            '$dni'
                        "
                    );
                    if (isset($siigoId)) {
                        $orderBd->add_order_note('Siigo success: La referencia de la factura en siigo es: ' .$siigoId);
                        $orderBd->save();
                    }
                }
                return $dataSiigo;
            }
        }
    }

    // get departament number
    public function getDepartamentNumber($abbreviation) {
        // Suponiendo que $this->departaments es tu arreglo de departamentos
        $departaments = $this->departaments;
        // Verificar si la abreviatura existe en el arreglo
        if (array_key_exists($abbreviation, $departaments)) {
            // Retornar el número asociado a la abreviatura
            return $departaments[$abbreviation]['numero'];
        } else {
            // Retornar un valor por defecto o manejar el caso cuando la abreviatura no existe
            return false;
        }
    }

    public function getCity($city, $state) {
		if ($city == 'Bogota' or $city == 'Bogotá') {
			$city = 'BOGOTÁ. D.C.';
		}
        // URL of the JSON data
        $jsonUrl = 'https://www.datos.gov.co/resource/gdxc-w37w.json';
        $jsonData = file_get_contents($jsonUrl);
        $citiesData = json_decode($jsonData, true);
        $cityClean = $this->removeAccents($city);
        foreach ($citiesData as $key => $value) {
            $municipality = $this->removeAccents($value['nom_mpio']);
			//var_dump($city);
            if ($value['cod_depto'] == $state and $cityClean == $municipality) {
                return $value['cod_mpio'];
                break;
            }
        }
        return false;
    }

    function removeAccents($str) {
        $accentedChars = ['á', 'é', 'í', 'ó', 'ú', 'ü', 'Á', 'É', 'Í', 'Ó', 'Ú', 'Ü', 'Ñ'];
        $nonAccentedChars = ['a', 'e', 'i', 'o', 'u', 'u', 'A', 'E', 'I', 'O', 'U', 'U', 'N'];
        $strWithoutAccents = strtr($str, array_combine($accentedChars, $nonAccentedChars));
        return strtolower($strWithoutAccents);
    }
    
    function calcularPorcentajeDescuento($precioOriginal, $precioFinal) {
        // Calcular el porcentaje de descuento
        $porcentajeDescuento = (1 - ($precioFinal / $precioOriginal)) * 100;
        return round($porcentajeDescuento, 2);
    }
}