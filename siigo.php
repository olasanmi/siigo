<?php 
	/*
	Plugin Name: Onexmedia Siigo
	Plugin URI: https://onexmedia.co
	Description: Plugins Para utilizar la Api de Siigo
	Version: 1.0
	Author: Onex Media
	Author URI: https://onexmedia.co
	License: GPL2
	*/

	defined( 'ABSPATH' ) || exit;

	define( 'SIIGO_NOMBRE', 'Siigo Api' );

	if ( ! defined( 'SIIGO_RUTA' ) ) {
		define( 'SIIGO_RUTA', plugin_dir_path(__FILE__));
	}

	include(SIIGO_RUTA.'/includes/horario.php');
	
	if ( ! function_exists( 'is_plugin_active' ) ){
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		//register_deactivation_hook(__FILE__, 'error_activacion');
	}
	if ( is_plugin_active('woocommerce/woocommerce.php') ){
		register_activation_hook(__FILE__,'activar');
		//registrando el hook menu
		add_action( 'admin_menu', 'sig_menu_administrador' );	
	}



	function activar(){
		define( 'SIGO_STATUS', 'actvado' );
		//conexion
		include(SIIGO_RUTA.'/includes/conexion.php');
		if(!ejecutarSQL::check_table('sg_credentials')){
			//Creamos la Tabla Crendenciales
			ejecutarSQL::create_sg_credentials_table();
		}
		if (!ejecutarSQL::check_table('sg_configuracion_jobs')){
			//creamos la tabla configuacion
			ejecutarSQL::create_sg_configuracion_jobs_table();	
		}
		if (!ejecutarSQL::check_table('sg_pedidos_jobs')){
			//creamos la tabla configuacion
			ejecutarSQL::create_sg_pedidos_jobs_table();	
		}
		if (!ejecutarSQL::check_table('sg_clients_jobs')){
			//creamos la tabla configuacion
			ejecutarSQL::create_sg_clients_jobs_table();	
		}
	}

	//Menu y Submenu
	function sig_menu_administrador()
	{
		add_menu_page(SIIGO_NOMBRE,SIIGO_NOMBRE,'manage_options',SIIGO_RUTA.'/admin/index.php');
	}

	// action y hooks de cordenes
	// add_action('woocommerce_thankyou', 'valdiateOnThank', 10, 3);
	// add_action('woocommerce_order_status_changed', 'changeOrderStatus', 10, 3);

	// Define la función que se ejecutará cuando cambie el estado de la orden
	function changeOrderStatus($order_id, $old_status, $new_status) {
		if (is_wc_order_edit_page()) {
			include(SIIGO_RUTA.'/orders/order.php');
			$orderClass = new OrderApi();
			$orderClass->sendOrder($order_id, $new_status);
		}
	}

	function valdiateOnThank($orderId) {
		if (is_wc_endpoint_url('order-received')) {
			include(SIIGO_RUTA.'/orders/order.php');
			$order = wc_get_order($orderId);
            $orderData = $order->get_data();
			$orderClass = new OrderApi();
			$orderClass->sendOrder($orderId, $orderData['status']);
		}
	}

	function is_wc_order_edit_page() {
		// Verificar si estamos en el panel de administración de WordPress
		if (!is_admin()) {
			return false;
		}
		// Verificar si estamos en la página de edición de una orden de WooCommerce
		if (isset($_GET['page']) && $_GET['page'] === 'wc-orders' && isset($_GET['action']) && $_GET['action'] === 'edit' && isset($_GET['id'])) {
			return true;
		}
		return false;
	}

	// button invoice
	add_action( 'woocommerce_admin_order_data_after_billing_address', 'buttonInvoice');
	function buttonInvoice($order) {
		echo '<a href="#" class="button-action" data-order-id="' . esc_attr($order) . '">Factura electrónica</a>';
		echo '<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>';
		echo "<script>jQuery(document).ready(function ($) {
			$('.button-action').on('click', function (e) {
				e.preventDefault();
				var button = $(this);
				var originalText = button.text();
				button.text('Generando factura...');
				var order_id = $(this).data('order-id');
				// Realiza la acción que necesitas, por ejemplo, una llamada AJAX
				$.post(ajaxurl, {
					action: 'generate_order_siigo',
					order_id: order_id
				}, function (response) {
					// Manejar la respuesta del backend
					button.text(originalText);
					if (response.status && response.status == 'error') {
						Swal.fire({
							title: 'Error!',
							text: response.message,
							icon: 'error'
						});
					}
					if (response.status && response.status == 'success') {
						Swal.fire({
							title: 'Felicidades!',
							text: response.message,
							icon: 'success'
						});
					}
				});
			});
		});</script>";
	}

	// button invoice action
	add_action('wp_ajax_generate_order_siigo', 'generate_order_siigo_function');
	function generate_order_siigo_function() {
		include(SIIGO_RUTA.'/orders/order.php');
		$order = wc_get_order($_POST['order_id']['id']);
        $orderData = $order->get_data();
		$orderClass = new OrderApi();
		$invoice = $orderClass->sendOrder($_POST['order_id']['id'], $orderData['status']);
		var_dump($invoice);
	}
?>