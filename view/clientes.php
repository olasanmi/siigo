<?php 

$jobs_pedidos = [];
$tipo = 'pedidos';
$mensaje = "";

    if(ejecutarSQL::check_table('sg_clients_jobs')){
        $consultas = ejecutarSQL::consultar('select * from sg_clients_jobs');
        $orders = mysqli_fetch_all($consultas, MYSQLI_ASSOC);
    }else{
        ejecutarSQL::create_sg_clients_jobs_table();
    }
    // obtenemos la configuracion previa
    $dataPreview = ejecutarSQL::consultar("select * from sg_configuracion_jobs where tipo='pedidos'");
    $document = '';
    $costCenter = '';
    $seller = '';
    $status = '';
    if (mysqli_num_rows($dataPreview) != 0){
        $list = mysqli_fetch_all($dataPreview, MYSQLI_ASSOC);
        foreach ($list as $key => $value) {
            $setting = json_decode($value['setting'], true);
            $document = $setting['document_id'];
            $costCenter = $setting['cost_center'];
            $seller = $setting['seller'];
            $status = $setting['status'];
        }
    }
?>

<div class="table100 ver1 m-b-110">
    <div class="table100-head">
        <table>
            <thead>
                <tr class="row100 head">
                    <th class="cell100 column1">Nombre</th>
                    <th class="cell100 column2">Apellido</th>
                    <th class="cell100 column1">Documento</th>
                    <th class="cell100 column1">Fecha</th>
                </tr>
            </thead>
        </table>
    </div>

    <div class="table100-body js-pscroll">
        <table>
            <tbody>
                <?php
                    if( count($orders) > 0 ){ 
                        foreach ($orders as $key => $order) {     
                ?>
                    <tr class="row100 body">
                        <td class="cell100 column1">
                            <?php
                                echo $order['nombre'];
                            ?>
                        </td>
                        <td class="cell100 column2">
                            <?php
                                echo $order['apellido'];
                            ?>
                        </td>
                        <td class="cell100 column1">
                            <?php  
                                echo $order['dni'];
                            ?>
                        </td>
                        <td class="cell100 column1">
                            <?php  
                                echo $order['fecha_creado'];
                            ?>
                        </td>
                    </tr>
                <?php 
                        } //for
                    } //cierre if
                ?>
            </tbody>
        </table>
    </div>
</div>