<?php 
    
    $jobs_pedidos = [];
    $tipo = 'pedidos';
    $mensaje = "";
    // auth
    $usuario = auth::user();
    // get data for selects
    // documents
    $urlTypesDocuments = 'https://api.siigo.com/v1/document-types?type=FV';
    $dataDocuments = RequestApi::request('GET', $urlTypesDocuments, true, $usuario['access_token'], null);
    
    // documents
    $urlCostCenter = 'https://api.siigo.com/v1/cost-centers';
    $dataCostCenters = RequestApi::request('GET', $urlCostCenter, true, $usuario['access_token'], null);

    // sellers
    $urlSeller = 'https://api.siigo.com/v1/users';
    $dataSellers = RequestApi::request('GET', $urlSeller, true, $usuario['access_token'], null);
    $dataSellers = json_decode(json_encode($dataSellers->results, true), true);

    // primary consult
    if(ejecutarSQL::check_table('sg_pedidos_jobs')){
        $consultas = ejecutarSQL::consultar('select * from sg_pedidos_jobs');
        $orders = mysqli_fetch_all($consultas, MYSQLI_ASSOC);
    }else{
        ejecutarSQL::create_sg_pedidos_jobs_table();
    }

    if ( isset($_POST['document_id']) && isset($_POST['cost_center'])  &&  isset($_POST['seller']) &&  isset($_POST['status'])) {
        $consultar = ejecutarSQL::consultar("select * from sg_configuracion_jobs where tipo='".$tipo."'");
        $fecha = $Object->format("Y-m-d h:i:s");

        $setting['document_id'] = $_POST['document_id'];
        $setting['cost_center'] = $_POST['cost_center'];
        $setting['seller'] = $_POST['seller'];
        $setting['status'] = $_POST['status'];
        $setting = json_encode($setting);

        if (mysqli_num_rows($consultar) != 0){
            consultasSQL::UpdateSQL('sg_configuracion_jobs', "
                setting = '".$setting."',
                fecha_editado = '".$fecha."'
            ", 'tipo="pedidos"');

            $mensaje = "Configuacion Actualizada";
        }else{ 
            consultasSQL::InsertSQL('sg_configuracion_jobs', 'tipo,	setting,fecha_creado,fecha_editado', "
                '$tipo',
                '$setting',
                '$fecha',
                '$fecha'
            ");

            $mensaje = "Configuacion agregada";
            
        }

        $class = 'updated';
    }
    // obtenemos la configuracion previa
    $dataPreview = ejecutarSQL::consultar("select * from sg_configuracion_jobs where tipo='pedidos'");
    $document = '';
    $costCenter = '';
    $seller = '';
    $status = '';
    if (mysqli_num_rows($dataPreview) != 0){
        $list = mysqli_fetch_all($dataPreview, MYSQLI_ASSOC);
        foreach ($list as $key => $value) {
            $setting = json_decode($value['setting'], true);
            $document = $setting['document_id'];
            $costCenter = $setting['cost_center'];
            $seller = $setting['seller'];
            $status = $setting['status'];
        }
    }
?>

<div class="table100 ver1 m-b-110">
    <div class="table100-head">
        <table>
            <thead>
                <tr class="row100 head">
                    <th class="cell100 column1">ID orden</th>
                    <th class="cell100 column2">Documento siigo</th>
                    <th class="cell100 column1">Fecha</th>
                </tr>
            </thead>
        </table>
    </div>

    <div class="table100-body js-pscroll">
        <table>
            <tbody>
                <?php
                    if( count($orders) > 0 ){ 
                        foreach ($orders as $key => $order) {     
                ?>
                    <tr class="row100 body">
                        <td class="cell100 column1">
                            <?php
                                echo $order['id_oder'];
                            ?>
                        </td>
                        <td class="cell100 column2">
                            <?php
                                echo $order['siigo_reference'];
                            ?>
                        </td>
                        <td class="cell100 column1">
                            <?php  
                                echo $order['fecha_creado'];
                            ?>
                        </td>
                    </tr>
                <?php 
                        } //for
                    } //cierre if
                ?>
            </tbody>
        </table>
    </div>
</div>
<?php  if( $mensaje != "" ) { ?>
        <div id="message"  class="<?php echo $class; ?> notice is-dismissible" style="margin: 15px 0 15px 0;">
            <p><?php echo $mensaje; ?></p>
            <button type="button" class="notice-dismiss close_div_message" onclick="closeMensaje(event)">
                <span class="screen-reader-text"> Descartar Este Aviso </span>
            </button>
        </div>
        <?php } ?>
<div property="content" typeof="Item"style="grid-template-columns: 1.5fr 2fr;">
    <div>
        <h3 property="headline" aria-label="Headline" class="">Configuración Pedidos</h3>
        <p property="text" aria-label="Text" class="">
             Es Importante agregar estos paramentros para asi sincronizar los pedidos de tu tienda,
             con siigo.
            <br>
            Tipo de Comprobante (Para verificar la configuración ir a Siigo Nube en el menú Configuración > Transacciones > Comprobantes > Facturas)
            <br>
            Centro de costo, el campo es obligatorio según la configuración del comprobante
        </p>
    </div>
    <div>
        <form action="<?php echo $_SERVER['REQUEST_URI'] ?>" class="__form_sg_api" method="POST" style="max-width: 380px;margin:0 auto;" >
            <div class="group_input">
                <label for="one" class="form-label">Comprobante de venta.</label>
                <select name="document_id" id="one"  class="form-control input_style_0" style="padding: 10px">
                    <?php
                        foreach ($dataDocuments as $doc => $documentData) {
                            ?>
                                <option
                                    value="<?php echo $documentData->id; ?>"
                                    <?php if(intval($documentData->id) == intval($document)) { ?> selected <?php } ?>
                                >
                                    <?php echo $documentData->name; ?>
                                </option>
                            <?php
                        }
                    ?>
                </select>
            </div>
            <div class="group_input">
                <label for="two" class="form-label">Centro de costo.</label>
                <select name="cost_center" id="two"  class="form-control input_style_0" style="padding: 10px">
                    <?php
                        foreach ($dataCostCenters as $doc => $costCenterS) {
                            ?>
                                <option
                                    value="<?php echo $costCenterS->id; ?>"
                                    <?php if(intval($costCenterS->id) == intval($costCenter)) { ?> selected <?php } ?>
                                >
                                    <?php echo $costCenterS->name; ?>
                                </option>
                            <?php
                        }
                    ?>
                </select>
            </div>
            <div class="group_input">
                <label for="tres" class="form-label">Vendedor asociado.</label>
                <select name="seller" id="tres"  class="form-control input_style_0" style="padding: 10px">
                    <?php
                        foreach ($dataSellers as $doc => $sellersSs) {
                            ?>
                                <option
                                    value="<?php echo $sellersSs['id']; ?>"
                                    <?php if(intval($sellersSs['id']) == intval($seller)) { ?> selected <?php } ?>
                                >
                                    <?php echo $sellersSs['first_name']; ?>
                                </option>
                            <?php
                        }
                    ?>
                </select>
            </div>
            <div class="group_input">
                <label for="tres" class="form-label">Estado en el que se enviaran las ordenes</label>
                <div class="row">
                    <div class="col-6">
                        Completados
                        <input <?php if($status === 'completed') { echo 'checked';}?> label="a" style="margin-top: -3px !important" type="radio" value="completed" class="form-control" name="status" id="cuatro">
                    </div>
                    <div class="col-6" style="margin-top: 10px !important">
                        En proceso
                        <input <?php if($status === 'processing') { echo 'checked';}?> style="margin-top: -3px !important" type="radio" value="processing" class="form-control" name="status" id="cuatro">
                    </div>
                </div>
            </div>
            <button type="submit" class="btn-submit-table" style="margin-top: 10px;cursor:pointer;">
                <span property="destination" aria-label="Destination" class="">Guardar</span>
            </button>
        </form>
    </div>
</div>